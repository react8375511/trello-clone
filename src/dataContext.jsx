import React, { createContext } from 'react';

const ApiContext = createContext();

export const useAPI = () => React.useContext(ApiContext);

export const ApiProvider = ({ children }) => {
  const api_key = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_API_TOKEN;
  return (
    <ApiContext.Provider value={{ api_key, token }}>
      {children}
    </ApiContext.Provider>
  );
};
