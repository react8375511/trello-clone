import { useEffect, useState } from 'react'
import Header from './components/header'
import Workspace from './components/Workspace'
import {Route,Routes} from 'react-router-dom'
import Board from './components/Board'
import { SnackbarProvider } from 'notistack';

function App() {
  
  return (
    <>
      <SnackbarProvider maxSnack={3} anchorOrigin={{ vertical: 'top', horizontal: 'right'}}>
        <section className='main-container'> 
          <Header/>
          <Routes>
            <Route path='/' element={
              <Workspace/>
            }></Route>

            <Route path='/:uid' element={
                <Board/>
            }></Route>
          </Routes>
        </section>

      </SnackbarProvider>
    </>
  )
}

export default App
