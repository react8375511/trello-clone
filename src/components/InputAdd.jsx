import {React, useEffect, useState } from 'react'
import { Box, Input, Stack } from '@mui/material';
import Typography from '@mui/material/Typography';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import Textarea from "@mui/joy/Textarea";
import Button from "./Btn";
import CloseIcon from '@mui/icons-material/Close';
import { useAPI } from '../dataContext'
import AddCard from './AddCard'
import axios from 'axios';






export default function InputAdd({placeholder,CardHide,Submit,btnName}) {

    const [name,setName]=useState();

    const handleSubmit = (e) => {
        e.preventDefault(); 
        Submit(name); 
        setName(''); 
    };
    // const handleKeyDown = (e) => {
    //     if (e.key === 'Enter' && !e.shiftKey) {
    //       handleSubmit(e);
    //     }
    //     if(e.key==='Escape' && !e.shiftKey){
    //         CardHide();
    //     }
    // };

  return (
    <form onSubmit={handleSubmit} style={{width:'fit-content',minHeight:'7rem'}}>
        <Box
            sx={{
                display: 'flex',
                marginTop:'2rem',
                flexDirection: 'column',
                justifyContent: 'space-between',
                // height: '100%',
            }}
            >
            <Textarea
            style={{
                width: 'auto',
                height: 'auto',
                resize: 'none',
                overflow: 'hidden',
            }}
            rows={1}
            width="100%"
            color="primary"
            minRows={2}
            placeholder={placeholder}
            size="sm"
            variant="soft"
            onKeyDown={(e) => {
                if (e.key === 'Enter' && !e.shiftKey) {
                  handleSubmit(e);
                }
              }}
            onChange={(e)=>setName(e.target.value)}
            sx={{ height: '35px'}}
            />
            <Box sx={{ display: 'flex', alignItems: 'center' ,marginTop:'1rem'}}>
                {console.log(name)}
                <AddCard 
                    onClick={()=>Submit(name)} btnName={btnName}
                    />
                <CloseIcon
                    sx={{ marginLeft: '1rem', cursor: 'pointer' }}
                    onClick={CardHide}
                    />
            </Box>
        </Box>
    </form>
  )
}
