
import { React,useEffect,useState } from 'react';
import { Box } from '@mui/material';
import Typography from '@mui/material/Typography';
import InputAdd from './InputAdd';
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import LinearProgress, { linearProgressClasses } from '@mui/material/LinearProgress';
import { styled } from '@mui/material/styles';
import ClearIcon from '@mui/icons-material/Clear';
import { useSnackbar } from 'notistack';
import CircularProgress from '@mui/material/CircularProgress';
import { fetchCheckItem,handlingPostItem,handlingDeleteItem,toggleCheck } from './Api_Fetch';

export default function Checkitems({checkListData,cardData}) {
    const {enqueueSnackbar}=useSnackbar();
    const [checkItemsData,setCheckItemsData]=useState([]);
    const [Check,ShowCheck]=useState(false);
    const [isLoading,setIsLoading]=useState(true);

    let progressPercentage=0;
    const calculateProgress = (items) => {
        if (items && items.length > 0) {
            const totalItems = items.length;
            let completedItemsCount = items.filter((item) => item.state === 'complete').length;
            progressPercentage = (completedItemsCount / totalItems) * 100;
        }
    };
    calculateProgress(checkItemsData);
    
    
    const show = () => {
        ShowCheck(true);
    }
    const CardHide=()=>{
        ShowCheck(false);
    }
    const label = { inputProps: { 'aria-label': 'Checkbox demo' } };
    const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
        height: 10,
        borderRadius: 5,
        [`&.${linearProgressClasses.colorPrimary}`]: {
          backgroundColor: theme.palette.grey[theme.palette.mode === 'light' ? 200 : 800],
        },
        [`& .${linearProgressClasses.bar}`]: {
          borderRadius: 5,
          backgroundColor: theme.palette.mode === 'light' ? '#1a90ff' : '#308fe8',
        },
      }));


    useEffect(()=>{
        async function getCheckItems(){
            try{
                const response=await fetchCheckItem(checkListData.id);
                
                const checkdata = await response.data;
                setCheckItemsData(checkdata);
                setIsLoading(false);
            }
    
            catch (error) {
                console.error('Error fetching data:', error);
                setIsLoading(false);
                enqueueSnackbar('Error fetching data!!!', { variant: 'error' });
            }
        }
    
        getCheckItems();
    },[])


    const sendcheck= async (name) => {
        if(name===undefined || name.trim()==='') return;
        try {
          const response = await handlingPostItem(checkListData.id,name);
        
          const responseData = response.data;
          setCheckItemsData((prev)=>[...prev,responseData])
        } catch (error) {
          console.error(error);
          enqueueSnackbar('Error sending data!!!', { variant: 'error' });
        }
        CardHide();
    };
    
    
    const toggleCheckBox = (eleID,isChecked,checkItemId) => {
        
        async function sendChecked(checkid){
            try{
                const response=await toggleCheck(cardData.id,checkid,isChecked);
                setCheckItemsData(prev => {
                    const itemprev= prev.map(item => {
                        if (item.id === eleID) {
                            return { ...item, state: isChecked ? "complete" : "incomplete" };
                        }else
        
                        return  item;
                    });
                    // calculateProgress(itemprev);
                    return itemprev;
                });
                // console.log(first)
            }
            
            catch (error) {
                console.error('Error fetching data:', error);
                enqueueSnackbar('Error sending data!!!', { variant: 'error' });
            }
        }
    
        sendChecked(checkItemId);
    };

    const deleteCheckItem=async(id)=>{
    
        try {
          const response = await handlingDeleteItem(checkListData.id,id);
        //   console.log('card deleted successfully:', response);
    
          const newdata=checkItemsData.filter((ele)=>{
              return ele.id!=id
          })
          setCheckItemsData(newdata);
          
        } 
        catch (error) {
          console.error('Error deleting list:', error);
          console.log('Error response:', error.response);
          enqueueSnackbar('Error deleting data!!!', { variant: 'error' });
        }
      }
    
    
  return (
    <>
    {isLoading ?(

        <Box sx={{ height:'20vh',display: 'flex' ,flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
            <CircularProgress sx={{color:'black'}}/>
        </Box>
        )
        : (
          <> 
    <Box>
        <Box sx={{width:'90%',display:'flex',alignItems:'center',justifyContent:'space-between',margin:'1rem'}}>
            {checkItemsData.length>0 ? (
                <>
            <Typography variant="h7">{progressPercentage.toFixed(1)}%</Typography>
            <BorderLinearProgress variant="determinate" value={progressPercentage} 
                sx={{
                    width:'88%',
                    '& .MuiLinearProgress-bar': {
                        backgroundColor: progressPercentage === 100.0 ? 'green' : 'blue',
                    },
                }}/></>):
                (<></>)}
        </Box>
        {checkItemsData.map((ele)=>{ 
            console.log(ele, "sdfs")
            return (
                <Box key={ele.id} sx={{ display: 'flex', alignItems: 'center',justifyContent:'space-between'}}>
                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                        <Checkbox {...label} color="success" checked={ele.state=="complete"} onClick={(e)=>toggleCheckBox(ele.id,e.target.checked,ele.id)}/>
                        <Typography variant="h6" sx={{textDecoration:ele.state==='complete' ? 'line-through' : 'none'}}>{ele.name}</Typography>
                    </Box>  
                    <ClearIcon onClick={()=>deleteCheckItem(ele.id)} sx={{transition:'color 0.3s','&:hover': {color:'red'},cursor:'pointer'}}/>
                </Box>
            )
        })}
        <Box sx={{height:'max',width:'50%',marginLeft:'2rem'}}>
        {!Check ? 
            (<Button 
                color="secondary" 
                sx={{
                    textTransform:'capitalize',
                    boxShadow: '0px 8px 15px rgba(0, 0, 0, 0.1)',
                    margin: '1rem 0 0 0',
                    borderRadius:'7px',
                    border:'2px solid transparent',
                    transition:'border 0.3s',
                    '&:hover': {border:'2px solid darkpink'},
                }}
                onClick={show}
                >Add an Item
            </Button>)
            :
            (<InputAdd placeholder={"Add an Item"}btnName={"Add an Item"} CardHide={CardHide} Submit={sendcheck}/>)
        }
        </Box>
    </Box>
    </>)}
    </>
  )
}
