import { React, useState } from 'react'
import { Box } from '@mui/material';
import Typography from '@mui/material/Typography';
import CardDetail from './CardDetail';

export default function Card({ cardData, deleteCard }) {

    const [openCardDialog, setOpenCardDialog] = useState(false);

    const handleClickOpen = () => {
        setOpenCardDialog(true);
    };
    return (
        <Box
            key={cardData.id}
            sx={{
                fontSize: '1rem',
                paddingTop: '1rem',
                textAlign: 'left',
                position: 'relative',

            }}
        >
            <Box sx={{
                display: 'inline-block',
                width: '100%',
                backgroundColor: 'lightgrey',
                borderRadius: '10px',
                cursor: 'pointer',
                border: '2px solid transparent',
                height: 'auto',
                transition: 'border 0.3s',
                '&:hover': {
                    border: '2px solid black',
                },

            }} onClick={handleClickOpen}>

                <Typography variant="h6" sx={{ margin: '0.5rem', wordWrap: 'break-word' }}>{cardData.name}</Typography>
            </Box>
            {openCardDialog && <CardDetail cardData={cardData} openCardDialog={openCardDialog} setOpenCardDialog={setOpenCardDialog} deleteCard={deleteCard} />}

        </Box>
    )
}


