import axios from "axios";

const api_key = import.meta.env.VITE_API_KEY;
const token = import.meta.env.VITE_API_TOKEN;

axios.defaults.params = {
  key: api_key,
  token: token,
};
export const getBoardsData = async () => {
  const response = await axios.get(
    "https://api.trello.com/1/members/me/boards"
  );
  return response;
};

export const postBoard = async (boardName) => {
  const response = await axios.post("https://api.trello.com/1/boards", {
    name: boardName,
  });
  return response;
};


export const handleDeleteBoards = async (id) => {
  const response = await axios.delete(
    `https://api.trello.com/1/boards/${id}`
  );
  return response;
};

export const handleBoardName = async (id) => {
    const response = await axios.get(
      `https://api.trello.com/1/boards/${id}`
    );
    return response;
};


export const fetchList = async (id) => {
  const response = await axios.get(
    `https://api.trello.com/1/boards/${id}/lists`
  );
  return response;
};

export const postingList = async (id, ListVal) => {
  const response = await axios.post(
    `https://api.trello.com/1/boards/${id}/lists?name=${ListVal}`
  );
  return response;
};

export const handlingDelete = async (listid) => {
  const deleteRes = await axios.put(
    `https://api.trello.com/1/lists/${listid}/closed?key=${api_key}&token=${token}&value=true`
  );
};

export const fetchingCards = async (id) => {
  const response = await axios.get(
    `https://api.trello.com/1/lists/${id}/cards?`
  );
  return response;
};

export const handlingDeleteCards = async (cardId) => {
  await axios.delete(`https://api.trello.com/1/cards/${cardId}?`);
};

export const handlePostCard = async (listId, cardName) => {
  const response = await axios.post(
    `https://api.trello.com/1/cards?name=${cardName}&idList=${listId}`
  );
  return response;
};

export const fetchCheckList = async (selectedId) => {
  const response = await axios.get(
    `https://api.trello.com/1/cards/${selectedId}/checklists`
  );
  return response;
};

export const handlingDeleteCheckList = async (id) => {
  await axios.delete(`https://api.trello.com/1/checklists/${id}`);
};

export const handleAddChecklist = async (selectedId, enterd) => {
  const response = await axios.post(
    `https://api.trello.com/1/cards/${selectedId}/checklists?key=${api_key}&token=${token}&name=${enterd}`
  );
  return response;
};

export const fetchCheckItem = async (id) => {
  const response = await axios.get(
    `https://api.trello.com/1/checklists/${id}/checkItems?`
  );
  return response;
};

export const handlingPostItem = async (id, newItem) => {
  const response = await axios.post(
    `https://api.trello.com/1/checklists/${id}/checkItems?name=${newItem}`
  );
  return response;
};

export const handlingDeleteItem = async (id, itemId) => {
  const response = await axios.delete(
    `https://api.trello.com/1/checklists/${id}/checkItems/${itemId}`
  );
  return response;
};

export const toggleCheck = async (selectedId, itemId, State) => {
  const response = await axios.put(
    `https://api.trello.com/1/cards/${selectedId}/checkItem/${itemId}?key=${api_key}&token=${token}&state=${State}`
  );
};
