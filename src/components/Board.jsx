import Cards from './Cards';
import {React, useEffect, useState } from 'react'
import { Box, Stack } from '@mui/material';
import { styled } from '@mui/material/styles';
import { Paper} from '@mui/material';
import Typography from '@mui/material/Typography';
import { useParams } from 'react-router-dom';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import DeleteIcon from '@mui/icons-material/Delete';
import Button from '@mui/material/Button';
import { blue } from '@mui/material/colors';
import axios from 'axios';
import {useNavigate} from 'react-router-dom'
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import InputAdd from './InputAdd';
import CircularProgress from '@mui/material/CircularProgress';
import { useSnackbar } from 'notistack';
import { fetchList,postingList,handlingDelete, handleBoardName } from './Api_Fetch';

const DemoPaper = styled(Paper)(({ theme }) => ({
    width: 250,
    height: 'auto',
    padding: theme.spacing(2),
    ...theme.typography.body2,
    textAlign: 'center',
}));


export default function Board() {
    const navigate = useNavigate();
    const { enqueueSnackbar } = useSnackbar();
    const {uid}=useParams();
    const [listdata,setListdata]=useState([]);
    
    const [showAddList, setShowAddList] = useState(false);
    const [boardName, setBoardName] = useState('');
    const [isLoading,setIsLoading]=useState(true);

    useEffect(()=>{
        async function getList(){
            try{
                const response=await fetchList(uid);

                const data = response.data;
                const boardResponse = await handleBoardName(uid);
                
                setBoardName(boardResponse.data.name);
                setListdata(data);
                setIsLoading(false);
            
            }

            catch (error) {
                console.error('Error fetching data:', error);
                setIsLoading(false);
                enqueueSnackbar('Error fetching data!!!', { variant: 'error' });
            }
        }

        getList();
    },[])

   

    const openListInput=()=>{
        setShowAddList(true);
    }
    const CardHide=()=>{
        setShowAddList(false);
    }

    
      


    const listSubmit = async (ListInput) => {
        if(ListInput===undefined || ListInput.trim()==='') return;
        try {
          const response = await postingList(uid,ListInput)
        
          const responseData = response.data;
          setListdata((prev)=>[...prev,responseData])
          enqueueSnackbar('list submitted successfully!!!', { variant: 'success' });
        } catch (error) {
          console.error(error);
          enqueueSnackbar('Error sending data!!!', { variant: 'error' });
        }
        CardHide();
    };



    const deleteList = async (listId) => {
        try {
            const response = await handlingDelete(listId);
            // console.log('List deleted successfully:', response);

            const newdata=listdata.filter((ele)=>{
                return ele.id!=listId
            })
            setListdata(newdata)
            enqueueSnackbar('list Deleted successfully!!!', { variant: 'success' });
        } catch (error) {
            console.error('Error deleting list:', error);
            console.log('Error response:', error.response);
            enqueueSnackbar('Error Deleting list!!!', { variant: 'error' });
        }
    }
        

  return (
    <>
        {isLoading ?(

            <Box sx={{ height:'80vh',display: 'flex' ,flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                <CircularProgress sx={{color:'white'}}/>
            </Box>
            )
            : (
        <>
        <Stack direction={'row'} spacing={2} sx={{display:'flex',justifyContent:'center',marginTop:'2rem'}}>
            <Box sx={{width:'93%',display:'flex',alignItems:'center'}} spacing={2}>
                <ArrowBackIcon sx={{cursor:'pointer',color:'white',fontSize:'2.3rem',transition: 'color 0.3s','&:hover': {color:'#2196F3'} }} onClick={()=>navigate('/')}/>
                <Typography variant="h3" sx={{color:'white',marginLeft:'2rem'}}>{boardName}</Typography>
            </Box>
            
        </Stack>
        
        <Stack direction={'row'} spacing={2} sx={{padding:'4rem' ,overflowY:'auto',maxWidth:'100vw',height:'68vh'} }>
            
            {listdata.map((data) => (
                    <DemoPaper
                    key={data.id}
                    square={false}
                    sx={{
                        borderRadius:'10px',
                        display: 'flex',
                        flexDirection:'column',
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                        height:'fit-content',
                        fontSize: '1rem',
                        minWidth:'14rem',
                        position:'relative'}}
                    >
                    <Box sx={{display: 'flex',alignItems:'center',justifyContent: 'space-between',width:'100%'}}>
                        <Typography variant="h6">{data.name}</Typography>
                        <DeleteIcon onClick={()=>deleteList(data.id)} sx={{cursor:'pointer','&:hover': {color:'red'},}}/>
                    </Box>
                    
                    <Cards uid={data.id}/>
                    </DemoPaper>
            ))}

            <Box sx={{height:'fit-content',backgroundColor:'#FFFFFF',borderRadius:'10px',padding:'1rem'}}>
                
                { !showAddList ? (<Box
                    key="add-list"
                    square={false}
                    sx={{
                        display: 'flex',
                        flexDirection:'column',
                        alignItems: 'center',
                        fontSize: '1rem',
                        cursor:'pointer',
                        transition: 'opacity 0.3s',
                        '&:hover': {
                            opacity: 0.8,
                        },
                    }}
                    onClick={openListInput}
                    >
                    <Box sx={{display:'flex',minWidth:'14rem'}}>
                        <AddCircleOutlineIcon sx={{fontSize:'2rem'}}/>
                        <Typography variant="h6" sx={{marginLeft:'1rem'}}>Add Another List</Typography>

                    </Box>
                </Box>) :
                    (
                    <InputAdd placeholder={"Add a List title..."} btnName={"Add List"} CardHide={CardHide} Submit={listSubmit}/>
                )}

            </Box>
            


        </Stack>
        </>
        )}
    </>
  )
}




const ColorButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(blue[500]),
    backgroundColor: blue[500],
    '&:hover': {
      backgroundColor: blue[700],
    },
  }));