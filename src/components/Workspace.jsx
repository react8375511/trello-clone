import React, { useState,useEffect } from 'react'
import { Paper,Box,Stack  } from '@mui/material';
import Typography from '@mui/material/Typography';
import { styled } from '@mui/material/styles';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import {useNavigate} from 'react-router-dom'
import InputAdd from './InputAdd';
import CircularProgress from '@mui/material/CircularProgress';
import { useSnackbar } from 'notistack';
import { getBoardsData ,postBoard,handleDeleteBoards} from './Api_Fetch';

const DemoPaper = styled(Paper)(({ theme }) => ({
    width: 120,
    height: 120,
    padding: theme.spacing(2),
    ...theme.typography.body2,
    textAlign: 'center',
  }));


export default function Workspace() {
    const { enqueueSnackbar } = useSnackbar();
    const navigate = useNavigate();

    const [mainData,setData]=useState([]);
    const [isLoading,setIsLoading]=useState(true);


    useEffect(() => {
        async function getData() {
          try {
            const response = await getBoardsData();
            
            const dd = response.data;
    
            // console.log(dd);
            setData(dd);
            setIsLoading(false);
            // enqueueSnackbar('Data fetched successfully!', { variant: 'success' });
          } catch (error) {
            console.error('Error fetching data:', error);
            setIsLoading(false);
            enqueueSnackbar('Error fetching data!!!', { variant: 'error' });
          }
        }
        getData();
    },[])


    const [open, setOpen] =useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const CardHide = () => {
        console.log("handleClose called");
        setOpen(false);
    };

    const handleSubmit = async (boarddata) => {
        if(boarddata===undefined || boarddata.trim()==='') return;
        try {
          const response = await postBoard(boarddata);
        
          const responseData = response.data;
          setData((prev)=>[...prev,responseData]);
        //   console.log(responseData);
        enqueueSnackbar('Board Created successfully!', { variant: 'success' });
        } catch (error) {
          console.error(error);
          enqueueSnackbar('Error Creating Board please try again later!!!', { variant: 'error' });
        }
        CardHide();
    };

    const deleteBoard=async(boardId)=>{
        try {
            const response = await handleDeleteBoards(boardId);
            
            const newdata=mainData.filter((ele)=>{
                return ele.id!==boardId;
            })

            // console.log(mainData);
            enqueueSnackbar('Board Deleted successfully!', { variant: 'success' });
            setData(newdata);
        }
        catch (error) {
            console.error(error);
            enqueueSnackbar('Error Deleting Board please try again later!!!', { variant: 'error' });
        }
    }

  return (
    <>
        <Box sx={{padding:'4rem'}}>

            <Typography variant="h4" gutterBottom sx={{color:'white', marginBottom:'4rem'}}> 
                Your Workspace
            </Typography>
            
            {isLoading ?(

                <Box sx={{ display: 'flex' ,justifyContent:'center'}}>
                    <CircularProgress sx={{color:'white'}}/>
                </Box>)
            : (
            
            <Stack direction={'row'} display={'flex'} gap={4} flexWrap={'wrap'}>

                {mainData.map((data) => (
                    <DemoPaper
                    key={data.id}
                    square={false}
                    sx={{
                        borderRadius:'10px',
                        height: '10rem',
                        width: '14rem',
                        fontSize: '1rem',
                        position:'relative',
                        display:'flex',
                        justifyContent:'center',
                        alignItems:'center',
                        transition: 'opacity 0.3s',
                        '&:hover': {
                            opacity: 0.8,},
                    }}
                    
                    >
                    <DeleteForeverIcon onClick={()=>deleteBoard(data.id)} sx={{fontSize:'2rem', position:'absolute',right:'0',cursor:'pointer',top:'0','&:hover': {color:'red'}}}/>
                    <Typography variant="h6" 
                        onClick={()=>navigate(`/${data.id}`)} 
                        sx={{width:'100%',
                            height:'83%',
                            cursor:'pointer',
                            display:'flex',
                            justifyContent:'center',
                            alignItems:'center',
                            
                        }}>{data.name}</Typography>
                    </DemoPaper>
                ))}
                <React.Fragment>
                    <DemoPaper 
                        square={false} 
                        sx={{
                            display:'flex',
                            flexDirection:'column',
                            justifyContent:'center',
                            alignItems:'center',
                            height:'10rem', 
                            width:'14rem',
                            fontSize:'1rem',
                            borderRadius:'10px',
                            padding:'0px',
                        }}>

                        {!open ? (        
                            <Typography 
                                variant="h6" 
                                onClick={handleClickOpen} 
                                sx={{
                                    height:'100%',
                                    width:'70%',
                                    display:'flex',
                                    justifyContent:'center',
                                    alignItems:'center',
                                    cursor:'pointer',
                                    padding:'2rem',
                                }}>Create a new board</Typography>):

                            (<InputAdd placeholder={"Add a new board title..."} btnName={"Add Board"} CardHide={CardHide} Submit={handleSubmit} sx={{padding:'1rem',marginTop:'0'}}/>)
                        }
                    </DemoPaper>
                </React.Fragment>
            </Stack>)}
        </Box>
    </>
  )
}
