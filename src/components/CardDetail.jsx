import * as React from 'react';
import Button from '@mui/material/Button';
// import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { styled } from '@mui/material/styles';
import { purple } from '@mui/material/colors';
import { useEffect,useState } from 'react';
import InputAdd from './InputAdd';
import { useAPI } from '../dataContext'
import { Box, Input, Stack } from '@mui/material';
import Typography from '@mui/material/Typography';

import DeleteIcon from '@mui/icons-material/Delete';
import CheckItems from './CheckItems'
import TaskAltIcon from '@mui/icons-material/TaskAlt';

import CancelIcon from '@mui/icons-material/Cancel';
import CircularProgress from '@mui/material/CircularProgress';
import { useSnackbar } from 'notistack';
import { fetchCheckList,handleAddChecklist,handlingDeleteCheckList } from './Api_Fetch';

export default function CardDetail({cardData,openCardDialog,setOpenCardDialog,deleteCard}) {
  
  const { enqueueSnackbar } = useSnackbar();
  const [checklistData,setChecklistData]=useState([]);
  const [showInput,setShowInput]=useState(false);
  const [isLoading,setIsLoading]=useState(true);

  const ColorButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(purple[500]),
    backgroundColor: purple[500],
    '&:hover': {
      backgroundColor: purple[700],
    },
  }));
  

  const CardHide=()=>{
    setShowInput(false);
  }
  const handleClose = () => {
    setOpenCardDialog(false);
  };

  useEffect(()=>{
    async function getChecklist(){
        try{
            
            const response=await fetchCheckList(cardData.id)
            const checkdata = await response.data;
            setChecklistData(checkdata);
            setIsLoading(false);
        }

        catch (error) {
            console.error('Error fetching data:', error);
            setIsLoading(false);
            enqueueSnackbar('Error fetching data!!!', { variant: 'error' });
        }
    }

    getChecklist();
  },[])



  const sendChecklist= async (name) => {
    if(name===undefined || name.trim()==='') return;
      try {
        const response=await handleAddChecklist(cardData.id,name);
        const responseData = response.data;
        setChecklistData((prev)=>[...prev,responseData])
        enqueueSnackbar('checklist sended successfully!!!', { variant: 'success' });
        // console.log(responseData);
      } catch (error) {
        console.error(error);
        enqueueSnackbar('Error sending data!!!', { variant: 'error' });
      }
      CardHide();
  };
  

  const deleteChecklist=async(id)=>{
    
    try {
      const response = await handlingDeleteCheckList(id)
      console.log('card deleted successfully:', response);

      const newdata=checklistData.filter((ele)=>{
          return ele.id!=id
      })
      setChecklistData(newdata);
      enqueueSnackbar('checklist deleted successfully!!!', { variant: 'success' });
    } 
    catch (error) {
      console.error('Error deleting list:', error);
      console.log('Error response:', error.response);
      enqueueSnackbar('Error deleting data!!!', { variant: 'error' });
    }
  }
  return (
    <React.Fragment>
      <Dialog
        open={openCardDialog}
        onClose={handleClose}
        PaperProps={{
          component: 'form',
          onSubmit: (event) => {
            event.preventDefault();
            handleClose();
          },
        }}  
        sx={{
          '& form': {
            width: '30rem',
            minHeight: '12rem',
          },
        }}
      >
        {isLoading ?(

          <Box sx={{ height:'20vh',display: 'flex' ,flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
              <CircularProgress sx={{color:'black'}}/>
          </Box>
          )
          : (
            <> 
        <DialogTitle sx={{display:'flex',alignItems:'center',justifyContent:'space-between',fontSize:'2rem',paddingBottom:'0',fontWeight:'600'}}>
            {cardData.name}
            <DeleteIcon onClick={()=>deleteCard(cardData.id)} sx={{cursor:'pointer','&:hover': {color:'red'},}}/>
        </DialogTitle>
        <DialogContent >
        
          {checklistData.map((ele)=>{
            // console.log(ele.name);

            return (
              <Box key={ele.id}>
                <Box
                  key={ele.id}
                  square={false}
                  sx={{
                      borderRadius:'10px',
                      display: 'flex',
                      flexDirection:'column',
                      justifyContent: 'flex-start',
                      alignItems: 'center',
                      height:'fit-content',
                      fontSize: '1rem',
                      minWidth:'14rem',
                      position:'relative'
                    }}
                  >
                  <Box sx={{display: 'flex',alignItems:'center',justifyContent:'space-between',width:'100%',marginTop:'1.5rem'}}>
                      <Box sx={{display:'flex',alignItems:'center'}}>
                        <TaskAltIcon/>
                        <Typography variant="h6" sx={{marginLeft:'1rem'}}>{ele.name}</Typography>
                      </Box>
                      
                      <CancelIcon sx={{
                        transition: 'color 0.3s',
                        '&:hover': { color: 'red', cursor: 'pointer' }
                        
                        }} 
                        onClick={()=>deleteChecklist(ele.id)}
                      />
                  </Box>
                </Box>

                
                {/* {console.log(cardData.id)} */}
                <CheckItems checkListData={ele} cardData={cardData}/>
              </Box>
            )
          }) }

          {!showInput ?
            <ColorButton variant="contained" onClick={()=>setShowInput(true)} sx={{marginTop:'1rem',textTransform:'capitalize',fontWeight:'600'}}>Add Checklist</ColorButton>
            :
            <InputAdd placeholder={"Add a checklist title..."} btnName={"Add Checklist"} CardHide={CardHide} Submit={sendChecklist}/>
          } 
        </DialogContent>
        <DialogActions >

          <Button onClick={handleClose}>Cancel</Button>
          <Button type="submit">Save</Button>
        </DialogActions>
        </>)}
      </Dialog>
    </React.Fragment>
  );
}