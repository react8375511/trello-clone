
import {React, useEffect, useState } from 'react'
import { Box, Input, Stack } from '@mui/material';
import Typography from '@mui/material/Typography';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import Card from './Card';
import InputAdd from './InputAdd';
import CircularProgress from '@mui/material/CircularProgress';
import { useSnackbar } from 'notistack';
import { fetchingCards,handlingDeleteCards,handlePostCard } from './Api_Fetch';

export default function Cards({uid}) {
    const { enqueueSnackbar } = useSnackbar();
    const [showAddCardInput, setShowAddCardInput] = useState(false);
    const [cardData,setCardData]=useState([]);
    const [isLoading,setIsLoading]=useState(true);

    const CardShow = () => {
        setShowAddCardInput(true);
    }

    const CardHide = () => {
        console.log("hide");
        setShowAddCardInput(false);
    };


    useEffect(()=>{
        async function getCardData(){
            try{
                const response=await fetchingCards(uid);
                const data = response.data;
                setCardData(data);
                setIsLoading(false);
            }

            catch (error) {
                console.error('Error fetching data:', error);
                enqueueSnackbar('Error fetching data!!!', { variant: 'error' });
            }
        }

        getCardData();
    },[])



    const cardSubmit = async (cardInput) => {
        if(cardInput===undefined || cardInput.trim()==='') return;
        try {
          const response = await handlePostCard(uid,cardInput);
        
          const responseData = response.data;
          setCardData((prev)=>[...prev,responseData])
          enqueueSnackbar('Card submitted successfully!!!', { variant: 'success' });
        //   console.log(responseData);
        } catch (error) {
          console.error(error);
          enqueueSnackbar('Error sending data!!!', { variant: 'error' });
        }
        CardHide();
    };


    const deleteCard = async (checkId) => {
        try {
            const response = await handlingDeleteCards(checkId);
            console.log('card deleted successfully:', response);
    
            const newdata=cardData.filter((ele)=>{
                return ele.id!=checkId
            })
            setCardData(newdata);
            enqueueSnackbar('Card deleted successfully!!!', { variant: 'success' });
            
        } catch (error) {
            console.error('Error deleting list:', error);
            console.log('Error response:', error.response);
            enqueueSnackbar('Error deleting data!!!', { variant: 'error' });
        }
        CardHide();
    }
  return (
    <>
        {isLoading ?(

        <Box sx={{height:'10vh',display: 'flex' ,flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
            <CircularProgress sx={{color:'black'}}/>
        </Box>
        )
        : (
            <>
        <Box 
            sx={{
                display: 'flex',
                flexDirection:'column',
                width:'100%',
                borderRadius:'10px',
                minWidth:'14rem',
            }}
            >
            {cardData.map((data) => (
                <Card key={data.id} cardData={data} deleteCard={deleteCard}/>
            ))}

            {!showAddCardInput ? (
                <Box sx={{
                    marginTop:'1.5rem',
                    padding:'1rem',
                    borderRadius:'10px',
                    width:'87%',
                    cursor:'pointer',
                    display:'flex',
                    transition: 'background-color 0.3s',
                    '&:hover': {
                        backgroundColor: 'rgba(0, 0, 0, 0.05)',
                    },}} onClick={CardShow}>
                    
                    <AddCircleOutlineIcon sx={{ fontSize: '1.3rem' }} />

                    <Typography variant="h7" sx={{ marginLeft: '1rem' }}>
                        Add a card
                    </Typography>
                </Box>
                ) : (
                
                <InputAdd placeholder={"Add a Card title..."} btnName={"Add Card"} CardHide={CardHide} Submit={cardSubmit}/>
                )}
        </Box>
        </>
        )}
    </>
  )
}
